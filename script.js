var Q = (q)=> document.querySelector(q);
var Qs = (q)=> document.querySelectorAll(q);


var gameRef;
var itemsRef;
var recepiesRef;
var processorsRef;

function setupStuff() {
    gameRef = firebase.database().ref('/games/factory')
    itemsRef = firebase.database().ref('/games/factory/items')
    itemsRef.on('value', function (snapshot) {    
        for(var i in snapshot.val()){
            new item(i);
        }
        loaded();
    });
    recepiesRef = firebase.database().ref('/games/factory/recipes')
    recepiesRef.on('value', function (snapshot) {    
        var recipes = snapshot.val();
        for(var key of Object.getOwnPropertyNames(recipes)){
            var r = recipes[key];
            new recipe(r.name,(!r.inputs)?[]:r.inputs.map(i=>{return {i:G.items[i.i],c:i.c};}),(!r.outputs)?[]:r.outputs.map(i=>{return {i:G.items[i.i],c:i.c};}),r.time);
        }
        loaded();
    });
    processorsRef = firebase.database().ref('/games/factory/processors')
    processorsRef.on('value', function (snapshot) {    
        var processors = snapshot.val();
        for(var key of Object.getOwnPropertyNames(processors)){
            var r = processors[key];
            new processor(r.name,r.recipes.map(i=>G.recipes[i]),r.maxNum);            
            //new recipe(r.name,(!r.inputs)?[]:r.inputs.map(i=>{return {i:G.items[i.i],c:i.c};}),(!r.outputs)?[]:r.outputs.map(i=>{return {i:G.items[i.i],c:i.c};}),r.time);
        }
        loaded();
    });
    shopItemsRef = firebase.database().ref('/games/factory/shopItems')
    shopItemsRef.on('value',function(snapshot){
        var shopItems = snapshot.val();
        for(var key of Object.getOwnPropertyNames(shopItems)){
            var si = shopItems[key];

            new shopItem(si.name,si.inputs.map(i=>{return {i:G[i.type+"s"][i.i],c:i.c};}),si.outputs.map(i=>{return {i:G[i.type+"s"][i.i],c:i.c};}))

        }
        loaded();
    });
    //new processor("tree",[G.recipes.create_wood],50);



    itemsRef.once('value').then(function (snapshot) {
        console.log("once value")
        console.log(snapshot.val())
    }).catch(function () {
        firebase.auth().signOut()
    });
}

function loading(){
    
}


function addUser(id, full, short, color) {
  //F.auth().currentUser.uid
}

var createItem = function(){
    openWindow({
        title:"Item",
        multi:[
            {name:"name",placeholder:"name"}
        ]
    }).then(w=>{
    new item(w.name);
    });
    
}

var createRecipe = function(){
    openWindow({
        title:"Recipe",
        multi:[
            {name:"name",placeholder:"name"},
            {name:"inputs",placeholder:"inputs (wood 10,iron 20)"},
            {name:"outputs",placeholder:"outputs"},
            {name:"time",placeholder:"time"}
        ]
    }).then(w=>{
    w.inputs = w.inputs.split(",").map(i=>i.trim()).map(i=>{var s = i.split(" ");return {i:G.items[s[0]],c:(s.length > 1)?parseInt(s[1]):1};})
    w.outputs = w.outputs.split(",").map(i=>i.trim()).map(i=>{var s = i.split(" ");return {i:G.items[s[0]],c:(s.length > 1)?parseInt(s[1]):1};})
    

    new recipe(w.name,w.inputs,w.outputs,parseInt(w.time))
    });
}

var createProcessor = function(){
    openWindow({
        title:"Processor",
        multi:[
            {name:"name",placeholder:"name"},
            {name:"recipes",placeholder:"recipes (wood_to_iron,wood_to_coal)"}
        ]
    }).then(w=>{
        new processor(w.name,w.recipes.split(",").map(i=>G.recipes[i.trim()]));
    });
}

var createShopItem = function(){
    openWindow({
        title:"ShopItem",
        multi:[
            {name:"name",placeholder:"name"},
            {name:"inputs",placeholder:"inputs (wood 10,iron 20)"},
            {name:"outputs",placeholder:"outputs"},
           ]
    }).then(w=>{
        w.inputs = w.inputs.split(",").map(i=>i.trim()).map(i=>{var s = i.split(" ");return {i:G.items[s[0]],c:(s.length > 1)?parseInt(s[1]):1};})
        w.outputs = w.outputs.split(",").map(i=>i.trim()).map(i=>{var s = i.split(" ");return {i:(s[0] in G.items)?G.items[s[0]]:G.processors[s[0]],c:(s.length > 1)?parseInt(s[1]):1};})
    
        new shopItem(w.name,w.inputs,w.outputs);
    });
    
}

/*
function drawUsers() {
  usersRef.once('value').then(function (snapshot) {
    users = snapshot.val();
    drawUsersDiv();
  });
  usersRef.on('value', function (snapshot) {
    users = snapshot.val();
    drawUsersDiv();
  });
}*/


function getQueryParams(qs) {
  qs = qs.split('+').join(' ');

  var params = {},
    tokens,
    re = /[?&]?([^=]+)=([^&]*)/g;

  while (tokens = re.exec(qs)) {
    params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
  }

  return params;
}
var query = getQueryParams(document.location.search);

pst = true;
async function login() {
  if ("user" in query && pst) {
    pst = false;
    var loginData = {
      email: query.user,
      password: query.pw
    }
  } else {
    var loginData = await openWindow({
      title: "Login:",
      multi: [{
          name: "email",
          placeholder: "Email Adresse"
        },
        {
          name: "password",
          placeholder: "Password",
          type: "password"
        }
      ]
    });
  }
  firebase.auth().signOut()
  var t = firebase.auth().signInWithEmailAndPassword(loginData.email, loginData.password).then(function (val) {
    setupStuff();
  }).catch(function (error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    console.log(errorCode)
    console.log(errorMessage)
    login()
    // ...
  });
}

firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    setupStuff();
  } else {
    login();
  }
});


var G = {};

G.items = {};
G.recipes = {};
G.processors = {};

G.inventory = {};
G.shopItems = [];
G.machines = [];
G.setFactory = (f)=>{
    if(G.currentFactory){
        G.currentFactory.machines.forEach(m=>m.hide());
    }

    if(f){
        G.currentFactory = f;
        G.currentFactoryType = f.type;
        G.currentFactory.machines.forEach(m=>m.show());
    }else{
        G.currentFactory = null;
        G.currentFactoryType = "main";
    }
}
G.currentFactory = null;
G.currentFactoryType = "main";
G.factoryTypes = {};
G.factorys = [];

G.now = ()=>Date.now();

class item {
    constructor(name){
        if(name in G.items)
            return;

        /** @type {string} */
        this.name = name;
        
        
        G.inventory[name] = 0;
        Q(".inventory").appendChild(this.div());
        G.items[name] = (this);
        new recipe("create_"+name,[],[{c:1,i:this}],5000)
        itemsRef.child("/"+name).set(name);
    }
    div(){
        var d = document.createElement("div");
        d.className = "item i_"+this.name;
        var a = document.createElement("div");
        a.className='amount i_'+this.name;
        a.innerText = 0;
       
        d.setAttribute("item",this.name);
        d.innerText = this.name;
        d.appendChild(a);
        return d;
    }
}

class recipe{
    constructor(name,inputs,outputs,time){
        if(name in G.recipes ){
            var tmp = G.recipes[name];
            if(
                tmp.name == name &&
                tmp.inputs == inputs &&
                tmp.outputs == outputs &&
                tmp.time == time
            ){
                return;
            }
        }


        this.name = name;
        this.inputs = inputs;
        this.outputs = outputs;
        this.time = time;
        G.recipes[name] = (this);
        gameRef.child("/recipes/"+name).set({
            name:this.name,
            inputs: inputs.map(i=>{return {i:i.i.name,c:i.c}}),
            outputs: outputs.map(i=>{return {i:i.i.name,c:i.c}}),
            time: this.time            
        });
    }
    create(inventory){
        if(this.inputs.every(inp=>{return inp.i.name in inventory && inventory[inp.i.name]>=inp.c;})){
            this.inputs.forEach(inp=>inventory[inp.i.name]-=inp.c);
            this.outputs.forEach(inp=>inventory[inp.i.name]+=inp.c);

        }
    }
}

class div{
    constructor(divC){
        this.div = divC();
    }
    hide(){
        this.div.style.display = "none";
    }
    show(){
        this.div.style.display = "";
    }
}

class machine extends div { 
    constructor(processor){
        this.processor = processor;
        this.activeId = 0;
        this.maxNum = processor.maxNum;
        this.active = processor.recipes[this.activeId];
        this.lastBuild = 0;
        super(this.createDiv);
        this.div.title = this.active.name;
        G.machines.push(this);
    }
    createDiv(){
        var m = document.createElement("div");
        m.className = 'machine';
        m.innerText = this.processor.name;
        m.onclick = ()=>{this.click();};
        m.oncontextmenu = (e)=>{e.preventDefault();this.destroy();};
        Q(".machines").appendChild(m);
        return m;
    }
    activate(receipe){
        if(this.processor.receipes.some(r=>r.name==receipe.name)){
            this.active = receipe;
        }
    }
    click(){
        this.next();
    }
    destroy(){
        G.machines.splice(G.machines.indexOf(this),1);
        this.div.parentElement.removeChild(this.div);
    }
    next(){
        this.activeId++;
        while(this.activeId>=this.processor.receipes.length){
            this.activeId-=this.processor.receipes.length;
        }
        this.active = this.processor.receipes[this.activeId];
        this.div.title = this.active.name;
    }
    update(){
        if(this.active && this.lastBuild+this.active.time < G.now()){
            this.active.create(G.inventory);
            this.maxNum--;
            this.lastBuild = G.now();
            if(this.maxNum == 0){
                this.destroy();
            }
        }
    }   
}

class shopItem extends div{
    constructor(name,inputs,outputs,allowedIn){
        if(G.shopItems.some(i=>i.name == name))
            return;
        
        this.allowedIn = (allowedIn)?allowedIn:[];
        
        this.name = name;
        this.inputs = inputs;
        this.outputs = outputs;
        gameRef.child("/shopItems/"+name).set({
            name:name,
            inputs:inputs.map(i=>{return{i:i.i.name,c:(i.c)?i.c:1,type:i.i.constructor.name}}),
            outputs:outputs.map(i=>{return{i:i.i.name,c:(i.c)?i.c:1,type:i.i.constructor.name}}),
            
        });
        super(this.createDiv);
        G.shopItems.push(this);
        Q(".shop").appendChild(this.div);
    }
    
    canCreate(inventory,factory){
        if(
            this.inputs.every(inp=>{return inp.i.name in inventory && inventory[inp.i.name]>=inp.c;}) && 
            (!factory || (!outputs.some(i=>i instanceof processor) || factory.canFit()))
            )
            return true;
    }
    /**
     * @param {*} inventory 
     * @param {factory} factory 
     */
    create(inventory,factory){
        if(this.canCreate(inventory,factory)){
            this.inputs.forEach(inp=>inventory[inp.i.name]-=inp.c);
            
            this.outputs.forEach(out=>{
                if(out.i instanceof item)
                    inventory[out.i.name]+=out.c;
                if(out.i instanceof processor)
                    factory.addMachine( new machine(G.processors[out.i.name]));
            });
       
        }
    }
    createAll(inventory,factory){
        var max = 100000;
        var n = 0;
        while(this.canCreate(inventory) && n<max){
            this.inputs.forEach(inp=>inventory[inp.i.name]-=inp.c);
            
            this.outputs.forEach(out=>{
                if(out.i instanceof item)
                    inventory[out.i.name]+=out.c;
                if(out.i instanceof processor)
                    factory.addMachine( new machine(G.processors[out.i.name]));
            });
            n++;
        }
    }
    update(inventory){
        if(this.canCreate(inventory) && (this.allowedIn.length == 0 || this.allowedIn.some(i=>i==G.currentFactoryType))){
            this.div.style.display = "";
        }else{
            this.div.style.display = "none";
        }
    }
    createDiv(){
        var d = document.createElement("div");
        d.className = "item buy";
        d.innerText = this.name;
        d.title = this.inputs.map((item)=>item.c+" x "+item.i.name).join(" + ")+" -> "+this.outputs.map((item)=>(("c" in item)?(item.c+" x "):"")+item.i.name).join(" + ")
        d.onclick = function(){
            this.create(G.inventory,G.currentFactory);
        }.bind(this);
        d.oncontextmenu = function(e){
            e.preventDefault();
            this.createAll(G.inventory,G.currentFactory);
        }.bind(this);
        return d;
    }
}


class clickers{
    constructor(name,out,inactive,reactivating){
        var c = document.createElement("div");
        c.className = "click";
        if(inactive>0){
            
        }
        
        c.setAttribute("item",out.i);
        c.setAttribute("amount",out.c);
        
        
        
        Q(".clickers").appendChild(c);
    }
}
                                 
class processor{
    constructor(name,receipes,maxNum){
        /** @type {string} */
        this.name = name;
        /** @type {recipe[]} */
        this.recipes = receipes;
        /** @type {recipe} */
        this.active = null;
        /** @type {number} */
        this.lastBuild = 0;
        /** @type {number} */
        this.maxNum = (maxNum)?maxNum:-1;
        gameRef.child("/processors/"+name).set({
            name:name,
            recipes:receipes.map(i=>i.name),
            maxNum:this.maxNum
        });
        G.processors[name] = (this);
    }    
}

class factoryType{
    constructor(name,maxPlaces){
        /** @type {string} */
        this.name = name;
        /** @type {number} */
        this.maxPlaces = maxPlaces;
        G.factoryTypes[name] = this;
    }
}

class factory extends div{
    constructor(factoryType){
        this.type = factoryType;
        this.maxPlaces = this.type.maxPlaces;
        this.machines = [];
        this.inventory = {};
        G.factorys.push(this);
        this.id = G.factorys.length;
        super(this.createDiv);
    }
    createDiv(){
        var d = document.createElement("div");
        d.className = "factory";
        d.innerText = this.type+"_"+this.id;
        document.querySelector(".factories").appendChild(d);
        d.onclick = function(){
            G.setFactory(this);
        }.bind(this)/*
        d.oncontextmenu = function(e){
            e.preventDefault();
            this.createAll(G.inventory);
        }.bind(this)*/
        return d;
    }
    update(){

    }
    canFit(){
        return (this.machines.length < this.maxPlaces);
    }
    addMachine(machine){
        if(this.canFit()){
            this.machines.push(machine);
            return true;
        }
        return false;
    }
}

tick = ()=>{
    var start = G.now();

    if(G.currentFactory){
        for(let m of G.currentFactory.machines){
            m.update();
        }
    }

    if(G.currentFactoryType == "main"){
        G.currentFactory = {};
        G.currentFactory.inventory = {};
        G.factorys.forEach(f=>{for(let key in f.inventory){if(!(key in G.currentFactory.inventory))G.currentFactory.inventory[key]=f.inventory[key];G.currentFactory.inventory[key]+=f.inventory[key]}});
    }
    for(let i in G.currentFactory.inventory){
        let c = G.currentFactory.inventory[i];
        if(c == 0){
            Q(".item.i_"+i).style.display = "none";    
        }else{
            Q(".item.i_"+i).style.display = "";
            Q(".amount.i_"+i).innerText = c;
        }
    }

    for(let s of G.shopItems){
        s.update(inventory);
    }
    var end = G.now();
    if((end-start) > 10)
        console.log("Delta: "+(end-start));
    setTimeout(function(){tick();},400);
    
};

var lParts = 4;
var lPartsLoaded = 0;
function loaded(){
    lPartsLoaded++;
    console.log("loaded "+lPartsLoaded);
    if(lPartsLoaded==lParts){
        console.log("Starting Server");
        tick();
        G.inventory.wood = 3000;
        
    }   
}

new factoryType("farm",64);
new factoryType("small",25);
new factoryType("mine",10);
new factory(G.factoryTypes.farm);

/*
new item("gold");
new item("wood");
new item("coal");
new item("energy");
new item("CO2");
new item("ironore");
new item("iron");*/
/*
new recipe("burn_wood_to_coal",[{c:10,i:G.items.wood}],[{c:1,i:G.items.coal}],1000)
new recipe("coal_to_energy",[{c:5,i:G.items.coal}],[{c:1,i:G.items.energy},{c:2,i:G.items.CO2}],1000)
new recipe("create_wood2",[],[{c:10,i:G.items.wood}],5000)
new recipe("ironore_to_iron",[{c:2,i:G.items.ironore},{c:1,i:G.items.coal}],[{c:1,i:G.items.iron},{c:1,i:G.items.CO2}],5000)
*/
 /*
new processor("tree",[G.recipes.create_wood],50);
new processor("tree2",[G.recipes.create_wood2]);
new processor("furnance",[G.recipes.burn_wood_to_coal,G.recipes.ironore_to_iron]);
new processor("generator",[G.recipes.coal_to_energy]);
new processor("mine",[G.recipes.create_ironore]);*/

//new shopItem("Give Wood",[],[{i:G.items.wood,c:100}])
    /*
new shopItem("Sell Wood",[{i:G.items.wood,c:10}],[{i:G.items.gold,c:1}])
new shopItem("Sell Energy",[{i:G.items.energy,c:5}],[{i:G.items.gold,c:50}])
new shopItem("Buy Tree",[{i:G.items.gold,c:3}],[{i:G.processors.tree}])
new shopItem("Buy Big Tree",[{i:G.items.gold,c:3000}],[{i:G.processors.tree2}])
new shopItem("Buy Furnance",[{i:G.items.gold,c:10}],[{i:G.processors.furnance}])
new shopItem("Buy Generator",[{i:G.items.gold,c:40}],[{i:G.processors.generator}])
new shopItem("Buy Mine",[{i:G.items.gold,c:100}],[{i:G.processors.mine}])*/
/*
var m = new machine(G.processors.furnance);
m.activate(G.receipes.burn_wood_to_coal);
*/

